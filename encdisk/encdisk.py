#!/usr/bin/env python3

"""usage: ./encdisk.py -h | --help | [OUTPUTS...]

Generates encoder disk outputs. If OUTPUTS... is omitted, all outputs
will be generated.
"""

import argparse
import math
import os
import sys

import cairo
import graycode # "graycode" in pip

OUTPUTS = {
    "quadrature_128": {
        "name": "quadrature_128_500dpi.png",
        "bits": 2, "steps": 128, "dpi": 500,
        "outer_rad":   25.0,
        "shaft_rad":    5.0,
        "track_start": 24.0,
        "track_width":  4.0,
        "track_gap":    2.0,
    },
    "quadrature_256": {
        "name": "quadrature_256_500dpi.png",
        "bits": 2, "steps": 256, "dpi": 500,
        "outer_rad":   25.0,
        "shaft_rad":    5.0,
        "track_start": 24.0,
        "track_width":  4.0,
        "track_gap":    2.0,
    },
    "quadrature_512": {
        "name": "quadrature_512_500dpi.png",
        "bits": 2, "steps": 512, "dpi": 500,
        "outer_rad":   25.0,
        "shaft_rad":    5.0,
        "track_start": 24.0,
        "track_width":  4.0,
        "track_gap":    2.0,
    },
    "quadrature_1024": {
        "name": "quadrature_1024_500dpi.png",
        "bits": 2, "steps": 1024, "dpi": 500,
        "outer_rad":   25.0,
        "shaft_rad":    5.0,
        "track_start": 24.0,
        "track_width":  4.0,
        "track_gap":    2.0,
    },
    "absolute_128": {
        "name": "absolute_256_500dpi.png",
        "bits": 7, "steps": 128, "dpi": 500,
        "outer_rad":   25.0,
        "shaft_rad":    5.0,
        "track_start": 24.0,
        "track_width":  1.8,
        "track_gap":    0.2,
    },
    "absolute_256": {
        "name": "absolute_256_500dpi.png",
        "bits": 8, "steps": 256, "dpi": 500,
        "outer_rad":   25.0,
        "shaft_rad":    5.0,
        "track_start": 24.0,
        "track_width":  1.8,
        "track_gap":    0.2,
    },
    "absolute_512": {
        "name": "absolute_512_500dpi.png",
        "bits": 9, "steps": 512, "dpi": 500,
        "outer_rad":   25.0,
        "shaft_rad":    5.0,
        "track_start": 24.0,
        "track_width":  1.8,
        "track_gap":    0.2,
    },
    "absolute_1024": {
        "name": "absolute_1024_500dpi.png",
        "bits": 10, "steps": 1024, "dpi": 500,
        "outer_rad":   25.0,
        "shaft_rad":    5.0,
        "track_start": 24.0,
        "track_width":  1.6,
        "track_gap":    0.1,
    },
}

def cairo_sector(ctx, x, y, r1, r2, th1, th2):
    ctx.set_line_width(abs(r2 - r1))
    ctx.arc(x, y, (r1 + r2) / 2, th1, th2)
    ctx.stroke()

def make_one(spec):
    DPI = spec["dpi"]
    DPMM = spec["dpi"] / 25.4

    width = int(round(2 * spec["outer_rad"] * DPMM))
    sfc = cairo.ImageSurface(cairo.Format.RGB24, width, width)
    ctx = cairo.Context(sfc)

    print(f"Generating {spec['name']}...")


    ctx.set_source_rgb(1.0, 1.0, 1.0)
    ctx.paint()

    ctx.set_source_rgb(0.5, 0.5, 0.5)

    # Put a crosshair in the middle - this is mostly for me to try things out
    # and improvise mounts on the bench, can probably be removed
    ch_rad = spec["shaft_rad"] * 0.4 * DPMM
    ctx.move_to(width//2 - ch_rad, width//2)
    ctx.line_to(width//2 + ch_rad, width//2)
    ctx.stroke()
    ctx.move_to(width//2, width//2 - ch_rad)
    ctx.line_to(width//2, width//2 + ch_rad)
    ctx.stroke()

    ctx.arc(width//2, width//2, spec["outer_rad"]*DPMM, 0, 2*math.pi)
    ctx.stroke()

    ctx.arc(width//2, width//2, spec["shaft_rad"]*DPMM, 0, 2*math.pi)
    ctx.stroke()

    ctx.set_source_rgb(0, 0, 0)

    steps = spec["steps"]
    bits  = spec["bits"]
    angle = (2 * math.pi)/spec["steps"]

    gcsteps = graycode.gen_gray_codes(bits) * (steps // 2**bits)

    # ok this is really hideous. i can't be bothered to write the code to
    # find the start and end of each long track, so i'm plotting them in single
    # step segments. this causes cairo's antialiasing to make a moire effect.
    # i just repaint it a couple times at a slight angle offset to get it nice
    # and black. someday i will be tried for my crimes, but that day is not
    # today suckers
    for n in range(3):
        for i, gc in enumerate(gcsteps):
            track_pos = spec["track_start"] - spec["track_width"]
            track_out = track_pos + spec["track_width"]
            th1 = angle * i + (n * 0.05 * angle)
            th2 = th1 + 1.02 * angle # overlap for no seams

            for b in range(bits):
                if gc & (1 << b):
                    cairo_sector(
                        ctx,
                        width//2, width//2,
                        track_pos * DPMM, track_out * DPMM,
                        th1, th2
                    )

                track_pos -= spec["track_width"] + spec["track_gap"]
                track_out -= spec["track_width"] + spec["track_gap"]

    sfc.write_to_png(spec["name"])

def main():
    ap = argparse.ArgumentParser(
        description="generate encoder disk outputs",
    )

    ap.add_argument("OUTPUTS", nargs="*",
                    help="preset outputs to make")
    ap.add_argument("--list-outputs", action="store_true",
                    help="list all preset outputs")

    ap.add_argument("-o", default=".", help="destination directory")
    args = ap.parse_args()

    if args.list_outputs:
        for k in OUTPUTS.keys():
            print(k)
        return

    os.chdir(args.o)

    if not args.OUTPUTS:
        args.OUTPUTS = list(OUTPUTS.keys())

    for i in args.OUTPUTS:
        if i not in OUTPUTS:
            print("unknown output:", i, file=sys.stderr)
            return

    for i in args.OUTPUTS:
        make_one(OUTPUTS[i])

if __name__ == "__main__":
    main()
