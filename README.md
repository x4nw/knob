# knob

it's knob™! (project in progress! you can't have one yet!)

(yeah idk maybe i'll give it a better name, but it's funnier if i don't)

knob™ is an oshw smooth optical rotary encoder control. It's meant to be
cheap and easy to build and feel really good.

Its features include:

- Very smooth rotation
- Built-in management of decoding and conversion to a numeric value
- Control over I2C or UART, with interrupt-on-change
- Haptic detents, "ticks", controllable resistance using a motor
- Rotating to match a value changed in software (this is not an absolute
  encoder, but it can rotate to show visually that a change happened)
- Target BOM cost under $10

## what's in here

- **encdisk** contains the printable design for the encoder disk. It
  should be printed on transparency and cut out. The script used to
  generate it is also here, and may be modified to customize the disk.

## making the parts

### encoder disk

The encoder disk should be printed on transparency film
([this one](https://www.amazon.com/gp/product/B078R4DGSB) for example), then
cut out on the printed lines. Make sure to print it at the right size — the
DPI value is mentioned in the filename to make this straightforward (hint:
GNU IMP on linux has a print dialog that can take DPI on the "image settings"
tab).
