#!/usr/bin/env python3

# fancysize --atpack= --mcu= FILE
# Print a nice fancy size report. Looks up the memory size in the atpack, so
# it works for chips avr-size doesn't know about. Alternatively, the size may
# be specified with --flash= and --sram=
#
# The output looks like this:
# ╭──────────────────────────────────────────────────────────────────────────╮
# │ MEMORY USAGE REPORT                                                      │
# ├──────────────────────────────────────────────────────────────────────────┤
# │ build/knob-fw.elf on ATtiny814                                           │
# │                                                                          │
# │ .text:   2048 B at 0x0 (flash)                                           │
# │ .rodata:   12 B at 0x8800 (mapped flash)                                 │
# │ .data:      0 B at 0x803E00 (sram, flash)                                │
# │ .bss:      66 B at 0x803E00 (sram)                                       │
# │ .eeprom:   10 B at 0x810000 (eeprom)                                     │
# │                                                                          │
# │ FLASH:   2060 B  25.1% ├████████────────────────────────┤ (8192 B total) │
# │ SRAM:      66 B  12.9% ├████────────────────────────────┤ ( 512 B total) │
# │ EEPROM:    10 B   7.8% ├██▌─────────────────────────────┤ ( 128 B total) │
# ╰──────────────────────────────────────────────────────────────────────────╯
#
# or in ASCII mode:
# ========================================================================
# MEMORY USAGE REPORT
# ------------------------------------------------------------------------
# build/knob-fw.elf on ATtiny814
#
# .text:   2048 B at 0x0 (flash)
# .rodata:   12 B at 0x8800 (mapped flash)
# .data:      0 B at 0x803E00 (sram, flash)
# .bss:      66 B at 0x803E00 (sram)
# .eeprom:   10 B at 0x810000 (eeprom)
#
# FLASH:   2060 B  25.1% [########------------------------] (8192 B total)
# SRAM:      66 B  12.9% [####----------------------------] ( 512 B total)
# EEPROM:    10 B   7.8% [##------------------------------] ( 128 B total)
# ------------------------------------------------------------------------

import argparse
import collections
import subprocess
import sys
import xml.etree.ElementTree

Section = collections.namedtuple("Section", ["name", "address", "size"])
Region  = collections.namedtuple("Region",  ["name", "sections"])

ARCS = True # make it even fancier haha

# Box drawing characters
B_TL_dblH = "╒"
B_H_dbl =   "═"
B_TR_dblH = "╕"
B_TL =      "┌"
B_H =       "─"
B_V =       "│"
B_TR =      "┐"
B_BL =      "└"
B_BR =      "┘"
B_LH =      "├"
B_HR =      "┤"
B_full =    "█"
B_half =    "▌"
B_empty =   "─"

if ARCS:
    B_TL_dblH = "╭"
    B_H_dbl =   "─"
    B_TR_dblH = "╮"
    B_TL =      "╭"
    B_TR =      "╮"
    B_BL =      "╰"
    B_BR =      "╯"

# In the future this script could be localizable?
def _(s):
    return s

def warn(s):
    print(_("WARNING:"), s, file=sys.stderr)

def get_size(sects, name):
    """Get a section size from a dict of sections, or 0 if not in the dict"""

    sect = sects.get(name)
    return 0 if sect is None else sect.size

def get_regions(sects, cap):
    """Given a dict of sections and a dict of capacities, return a dict of
    regions.

    This function is currently AVR-specific, and could be extended to support
    other microcontrollers.
    """

    regions = {
        "flash":  Region("flash",  [".text", ".rodata", ".data"]),
        "sram":   Region("sram",   [".data", ".bss"]),
        "eeprom": Region("eeprom", [".eeprom"]),
    }

    # AVR-specific: .rodata generally goes into SRAM, but on newer parts that
    # provide a mirror of the flash into data space, it is only in flash.

    include_rodata = True
    if "mapped_progmem" in cap:
        if ".rodata" in sects:
            mpstart = cap["mapped_progmem_start"]
            mplen   = cap["mapped_progmem"]
            rodata_start = sects[".rodata"].address
            if (rodata_start >= mpstart and rodata_start < (mpstart + mplen)):
                include_rodata = False
        else:
            # We don't have anything in .rodata, but it'd probably be mapped
            # correctly anyway. List it so the text looks correct to the user
            include_rodata = False

    if include_rodata:
        regions["sram"].sections.append(".rodata")

    return regions

def get_sections(obj, avr_size):
    p = subprocess.run(
        [avr_size, "-Ad", obj],
        capture_output=True,
    )
    sects = {}
    for line in p.stdout.decode("ascii").split("\n"):
        if not line.startswith("."):
            continue
        sect, size, addr = line.split()
        sects[sect] = Section(sect, int(addr), int(size))
    return sects

def get_capacity(atpack, mcu):
    path = f"{atpack}/atdf/{mcu}.atdf"

    try:
        root = xml.etree.ElementTree.parse(path)
    except FileNotFoundError:
        warn(_("unable to locate device definition at {path}, "
               "usage percentages will be missing").format(path=path))
        return {}

    segments = root.findall(
        f"./devices/device[@name='{mcu}']"
        + f"/address-spaces/address-space/memory-segment"
    )

    cap = {}
    for i in segments:
        if i.attrib["type"] == "ram":
            cap["sram"] = int(i.attrib["size"], 0)

        if i.attrib["type"] == "flash":
            cap["flash"] = int(i.attrib["size"], 0)

        if i.attrib["type"] == "eeprom":
            cap["eeprom"] = int(i.attrib["size"], 0)

        if i.attrib["name"] == "MAPPED_PROGMEM":
            # TODO storing this info this way is a bit of a hack
            cap["mapped_progmem"] = int(i.attrib["size"], 0)
            cap["mapped_progmem_start"] = int(i.attrib["start"], 0)

    if "sram" not in cap or "flash" not in cap:
        warn(_("some extent sizes couldn't be parsed from {path}, "
               "usage percentages may be missing").format(path=path))

    return cap

def usage_bar(value, total, width=32, ascii=False):
    if ascii:
        left  = "["
        right = "]"
        bar   = "-"
        half  = "-"
        full  = "#"
    else:
        left  = B_LH
        right = B_HR
        bar   = B_H
        half  = B_half
        full  = B_full

    if value > total:
        return left + ">" * (width + 1)
    else:
        blocks = width * value / total
        fullblocks = int(blocks)
        halfblocks = 1 if (blocks - fullblocks) >= 0.5 else 0
        return (left +
                full * fullblocks +
                half * halfblocks +
                bar * (width - fullblocks - halfblocks) +
                right)

def int_size(v):
    v = v.lower()
    if v.endswith("k"):
        v = v[:-1]
        mult = 1024
    elif v.endswith("m"):
        v = v[:-1]
        mult = 1048576
    else:
        mult = 1

    return int(v, 0) * mult

ap = argparse.ArgumentParser(
    description=_("Print a size report for an AVR binary")
)
ap.add_argument("--atpack", help=_("path to extracted .atpack file"))
ap.add_argument("--mcu", help=_("name of microcontroller"))
ap.add_argument("--flash", type=int_size, help=_("amount of flash in bytes"))
ap.add_argument("--sram", type=int_size, help=_("amount of SRAM in bytes"))
ap.add_argument("--size-path", default="avr-size", help=_("path to avr-size"))
ap.add_argument("--ascii", action=argparse.BooleanOptionalAction,
                help=_("only use ASCII characters to format display"))
ap.add_argument("FILE", help=_("ELF file to process"))
args = ap.parse_args()

cap = {}

if args.atpack is not None:
    cap.update(get_capacity(args.atpack, args.mcu))

if args.flash is not None:
    cap["flash"] = args.flash

if args.sram is not None:
    cap["sram"] = args.sram

sects = get_sections(args.FILE, avr_size=args.size_path)
regions = get_regions(sects, cap)

def section_row(name):
    sect_regions = [i for i in regions.values() if name in i.sections]
    if name in sects:
        addr_text = f"at 0x{sects[name].address:X} "
    else:
        addr_text = ""
    return {
        "name": name,
        "used": get_size(sects, name),
        "text": f"{addr_text}({', '.join(_(i.name) for i in sect_regions)})"
    }

rows = [
    {"text": f"{args.FILE} on {args.mcu or 'unknown chip'}"},

    {},
    section_row(".text"),
    section_row(".rodata"),
    section_row(".data"),
    section_row(".bss"),
    section_row(".eeprom"),

    {},
    {"name": _("FLASH"),  "region": regions["flash"], "size": cap.get("flash")},
    {"name": _("SRAM"),   "region": regions["sram"], "size": cap.get("sram")},
    {"name": _("EEPROM"), "region": regions["eeprom"], "size": cap.get("eeprom")},
]

for i in rows:
    if "region" in i:
        i["used"] = sum(get_size(sects, j) for j in i["region"].sections)

name_align = max(len(i["name"]) for i in rows if "name" in i)
used_align = max(len(str(i["used"])) for i in rows if "used" in i)
try:
    size_align = max(len(str(i["size"])) for i in rows if i.get("size"))
except ValueError:
    size_align = None

# doublebar (===) and singlebar (---) are entered as placeholders now, then
# generated at the end once we know the final width
title = _("MEMORY USAGE REPORT")
doublebar = []
singlebar = []
lines = [
    doublebar,
    [title],
    singlebar,
]

for i in rows:
    line = []
    lines.append(line)

    if i.get("name"):
        line.append(f"{(i['name']+':'):<{name_align+1}}")

    if "used" in i and i.get("size") is not None:
        line.append(f"{i['used']:>{used_align}} B")

        if i["size"]:
            pct = 100 * i["used"] / i["size"]
            line.extend([
                f"{pct:5.1f}%",
                usage_bar(i["used"], i["size"], ascii=args.ascii),
                f"({i['size']:>{size_align}} B {_('total')})",
            ])
    elif "used" in i:
        line.append(f"{i['used']:>{used_align}} B")


    if i.get("text"):
        line.append(i["text"])

lines.append(singlebar)

width = max(len(i) - 1 + sum(len(j) for j in i) for i in lines)
#           ^^^^^^^^^|   ^^^^^^^^^^^^^^^^^^^^^|
#                    |                        |___ length of all fields
#                    |____________________________ spaces between fields

doublebar.append(("=" if args.ascii else B_H_dbl) * width)
singlebar.append(("-" if args.ascii else B_H) * width)

for n, line in enumerate(lines):
    if args.ascii:
        left = ""
        right = ""
    elif n == 0:
        left = B_TL_dblH + B_H_dbl
        right = B_H_dbl + B_TR_dblH
    elif n == 2:
        left = B_LH + B_H
        right = B_H + B_HR
    elif n == len(lines) - 1:
        left = B_BL + B_H
        right = B_H + B_BR
    else:
        left = B_V + " "
        right = " " + B_V

    text = " ".join(line)
    text += (" " * (width - len(text)))
    print((left + text + right).strip())
