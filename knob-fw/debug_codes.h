// intellectual property is bullshit bgdc

#ifndef DEBUG_CODES_H
#define DEBUG_CODES_H

#define DBG_EEPSTAT 0x01 // General EEPROM statuses
#define DBG_EEPSTAT_WRITING 0x0001 // writing...
#define DBG_EEPSTAT_BAD_MAGIC 0x0002 // loading defaults because bad magic
#define DBG_EEPSTAT_BAD_CSUM  0x0003 // loading defaults because bad checksum
#define DBG_EEPCSUM 0x02 // Emitting the EEPROM checksum on write/read

#define DBG_ERROR 0xFF
#define DBG_ERROR_UART 0x0000

#endif // !defined(DEBUG_CODES_H)
