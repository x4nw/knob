#include "uart.h"
#include "debug_codes.h"
#include "hw.h"
#include "regs.h"

#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <util/atomic.h>
#include <util/delay.h>
#include <xringbuf.h>

#define QUEUE_LEN 16
static xringbuf_t uart_in;
static xringbuf_t uart_out;

static uint8_t _uart_in_buf[QUEUE_LEN];
static uint8_t _uart_out_buf[QUEUE_LEN];

static uint8_t _ctrla_idle;
static bool _error, _error_announced, _error_sticky;

static uint16_t _accumulator = 0;
static uint8_t _addr = 0;

// Pull this into xavrstuff!
#define MIN_BAUD 0x40
#define CLAMPU16(v) ((v) > 65535 ? 65535 : (v))
#define BAUDVAL_1X(v) CLAMPU16((64uLL * F_CPU) / (16uLL * (v)))
#define BAUDVAL_2X(v) CLAMPU16((64uLL * F_CPU) / ( 8uLL * (v)))
#define NEED_2X (BAUDVAL_1X(115200) < MIN_BAUD)

// Convert bytes in a packet to hex. They are expected to be stuffed like:
// data:   A   B   C
// index:  0 1 2 3 4 5 len
//
// and will be converted to:
// data:   a a b b c c
// index:  0 1 2 3 4 5 len
static void _convert_hex(uint8_t * packet, uint8_t len);
static void _send_byte(uint8_t b);
static void _send_hex(uint8_t b);

static uint16_t const _bauds_1x[] = {
	[REGD_BAUD_9600] = BAUDVAL_1X(9600),
	[REGD_BAUD_19200] = BAUDVAL_1X(19200),
	[REGD_BAUD_28800] = BAUDVAL_1X(28800),
	[REGD_BAUD_38400] = BAUDVAL_1X(38400),
	[REGD_BAUD_57600] = BAUDVAL_1X(57600),
	[REGD_BAUD_115200] = BAUDVAL_1X(115200),
};
#if NEED_2X
static uint16_t const _bauds_2x[] = {
	[REGD_BAUD_9600] = BAUDVAL_2X(9600),
	[REGD_BAUD_19200] = BAUDVAL_2X(19200),
	[REGD_BAUD_28800] = BAUDVAL_2X(28800),
	[REGD_BAUD_38400] = BAUDVAL_2X(38400),
	[REGD_BAUD_57600] = BAUDVAL_2X(57600),
	[REGD_BAUD_115200] = BAUDVAL_2X(115200),
};
#endif
static size_t const _num_bauds = sizeof(_bauds_1x)/sizeof(_bauds_1x[0]);

void
uart_init(bool rxen, bool alternate_tx, bool debug_en)
{
	(void) rxen;
	(void) alternate_tx;
	(void) debug_en;

	xringbuf_init(&uart_in,  _uart_in_buf,  sizeof(_uart_in_buf));
	xringbuf_init(&uart_out, _uart_out_buf, sizeof(_uart_out_buf));

	_error = false;
	_error_announced = false;
	_error_sticky = false;
	_accumulator = 0;
	_addr = 0;
	_ctrla_idle  = rxen ? USART_RXCIE_bm : 0;

	uint8_t ctrlb = (rxen ? USART_RXEN_bm : 0)
	              | USART_TXEN_bm;
	uint8_t ctrlc = USART_CMODE_ASYNCHRONOUS_gc
	              | USART_PMODE_ODD_gc
	              | USART_CHSIZE_8BIT_gc;

	uint8_t nbaud = REG_READ(REGD_BAUD);
	if (nbaud >= _num_bauds)
	{
		nbaud = _num_bauds;
		// TODO signal an invalid config somehow!
	}

	uint16_t baud = _bauds_1x[nbaud];

#if NEED_2X
	if (baud < MIN_BAUD)
	{
		baud = _bauds_2x[nbaud];
		ctrlb |= USART_RXMODE_CLK2X_gc;
	}
#else
	baud = _bauds_1x[nbaud];
#endif

	ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
	{
		if (alternate_tx)
		{
			XPIN_USART_ALT_CFG(P_UART_TX_ALT);
			XPIN_OUT1(P_UART_TX_ALT);
			XPIN_DRIVE(P_UART_TX_ALT);
		}
		else
		{
			XPIN_USART_ALT_CFG(P_UART_TX);
			XPIN_OUT1(P_UART_TX);
			XPIN_DRIVE(P_UART_TX);

			XPIN_CFG(P_UART_RX, XPULLUP);
			XPIN_RELEASE(P_UART_RX);
		}

		USART0.BAUD  = baud;
		USART0.CTRLC = ctrlc;
		USART0.CTRLB = ctrlb;
		USART0.CTRLA = _ctrla_idle;
	}


	// TODO toggle the pin driver when emitting from the uart. This only
	// needs to be done in main mode, there's no point doing it on the
	// alternate port. (Can we use RS485 mode? SEE ERRATA - need to set
	// pin mode to INPUT for this)
}

void
uart_deinit(void)
{
	// We want to wait for the outbox to be empty so any final ack message
	// goes out, but make sure to only do this if the USART is actually
	// running!

	// TODO deinit the pins!

	bool running;
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
	{
		uint8_t ctrla = USART0.CTRLA;
		uint8_t ctrlb = USART0.CTRLB;
		// No new input!
		USART0.CTRLA = ctrla & ~(USART_RXCIE_bm);
		running = (ctrla & USART_DREIE_bm)
		       && (ctrlb & USART_TXEN_bm);
	}

	if (running)
	{
		// TODO - we should have a function to call during long blocks
		// like this where any housekeeping that needs to run can be
		// put
		while (!xringbuf_empty(&uart_out));
		// Wait for the ISR to clear DREIE
		while (USART0.CTRLA & USART_DREIE_bm);
		// Wait for the transmission to be *done*
		while (!(USART0.STATUS & USART_TXCIF_bm));
	}

	ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
	{
		USART0.CTRLA = 0;
		USART0.CTRLB = 0;
	}
}

void
uart_handle(void)
{
	uint8_t c;

	if (_error && !_error_announced)
	{
		uart_debug(DBG_ERROR, DBG_ERROR_UART);
		_error_announced = true;
	}

	if (!xringbuf_pop_one(&uart_in, &c))
	{
		return;
	}

	if (c >= '0' && c <= '9')
	{
		_accumulator <<= 4;
		_accumulator |= c - '0';
	}
	else if (c >= 'A' && c <= 'F')
	{
		_accumulator <<= 4;
		_accumulator |= c - 'A' + 0xA;
	}
	else if (c == 'd') // address match
	{}
	else if (c == 'p' /* ping */ || c == 'c' /* clear error */)
	{
		_send_byte('+');
	}
	else if (c == '+' || c == '-') // ack or nak - do nothing
	{}
	else if (c == '-') // debug, not implemented - send nak
	{
		_send_byte('-');
	}
	else if (c == 'a')
	{
		_addr = _accumulator & 0xFF;
	}
	else if (c == 'r')
	{
		// read N bytes, then ETX
		_accumulator &= 0xFF;
		while (_accumulator--)
		{
			_send_hex(reg_read(_addr++));
		}
		_send_byte('.');
	}
	else if (c == 'W' || c == 'w')
	{
		if (c == 'W')
			reg_write(_addr++, _accumulator >> 8);

		reg_write(_addr++, _accumulator & 0xFF);
		_send_byte('+');
	}
}

void
uart_debug(uint8_t key, uint16_t value)
{
	uint8_t dbg_packet[8];
	dbg_packet[0] = '!';
	dbg_packet[1] = key;
	dbg_packet[3] = value >> 8;
	dbg_packet[5] = value & 0xFF;
	dbg_packet[7] = '.';
	_convert_hex(&dbg_packet[1], 6);

	if (xringbuf_push(&uart_out, dbg_packet, sizeof(dbg_packet)))
	{
		USART0.CTRLA = _ctrla_idle | USART_DREIE_bm;
	}
}

static void
_send_byte(uint8_t b)
{
	// TODO feed watchdog once before pushing
	while (!xringbuf_push(&uart_out, &b, 1));
	USART0.CTRLA = _ctrla_idle | USART_DREIE_bm;
}

static void
_send_hex(uint8_t b)
{
	uint8_t pkt[] = {b, 0};
	_convert_hex(pkt, 2);

	// TODO feed watchdog once before pushing
	while (!xringbuf_push(&uart_out, pkt, sizeof(pkt)));
	USART0.CTRLA = _ctrla_idle | USART_DREIE_bm;
}

// The way this function is written is a little silly. I'm sorry.......had some
// fun microöptimizing it because I was bored. This seems to be the smallest
// way to write this
__attribute__((optimize("-Os")))
static void
_convert_hex(uint8_t * packet, uint8_t len)
{
	for (uint8_t i = 0; i < len; )
	{
		uint8_t b = packet[i];
		uint8_t c;

		c = b >> 4;
		c += '0';
		if (c > '9') c += ('A' - '9' - 1);
		packet[i++] = c;

		c = b & 0xF;
		c += '0';
		if (c > '9') c += ('A' - '9' - 1);
		packet[i++] = c;
	}
}

ISR(USART0_RXC_vect)
{
	uint8_t stat = USART0.RXDATAH;
	uint8_t data = USART0.RXDATAL;

	if (stat & (USART_BUFOVF_bm | USART_FERR_bm | USART_PERR_bm))
	{
		_error = true;
		_error_sticky = true;
	}
	else if (_error)
	{
		if (data == 'c')
			_error = false;
	}
	else
	{
		xringbuf_push_one_i(&uart_in, &data);
	}
}

ISR(USART0_DRE_vect)
{
	uint8_t c;
	bool have_data = xringbuf_pop_one_i(&uart_out, &c);

	if (have_data)
		USART0.TXDATAL = c;
	else
		USART0.CTRLA = _ctrla_idle;
}
