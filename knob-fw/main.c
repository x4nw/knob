#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>

#include <avr/cpufunc.h>
#include <avr/io.h>
#include <avr/interrupt.h>

#include "debug_codes.h"
#include "regs.h"
#include "uart.h"
#include "hw.h"

FUSES = {
	.WDTCFG = WDT_WINDOW_OFF_gc | WDT_PERIOD_OFF_gc,
	.BODCFG = LVL_BODLEVEL2_gc /* 2.6V */
	        | BOD_ACTIVE_ENABLED_gc
	        | BOD_SLEEP_SAMPLED_gc,

#if ((F_CPU == 16000000) || (F_CPU == 8000000) || (F_CPU == 4000000) || \
     (F_CPU ==  2000000) || (F_CPU == 1000000))

	.OSCCFG = FREQSEL_16MHZ_gc,

#elif ((F_CPU == 20000000) || (F_CPU == 10000000) || (F_CPU == 5000000) || \
       (F_CPU ==  2500000) || (F_CPU ==  1250000))

	.OSCCFG = FREQSEL_20MHZ_gc,

#else
#  error "unexpected CPU frequency"
#endif

	.TCD0CFG = 0,
	.SYSCFG0 = CRCSRC_NOCRC_gc | RSTPINCFG_UPDI_gc | FUSE_EESAVE_bm,
	.SYSCFG1 = SUT_1MS_gc,
	.APPEND  = 0,
	.BOOTEND = 0,
};

static void _handle_debug_commands(void);
static void _handle_reset(void);
static void _reset(void);
static void _take_system_awake(void);

int
main(void)
{
	for (;;)
	{
		_handle_reset();
		_handle_debug_commands();
		uart_handle();

		// Copy the ISR count into REG_ACTUAL
		// Filter REG_ACTUAL into REG_VALUE
	}

	return 0;
}

static void
_handle_debug_commands(void)
{
	uint8_t cmd = REG_READ(REGD_COMMAND);

	switch (cmd)
	{
	case REGD_COMMAND_COMMIT: regs_commit_debug(); break;
	case REGD_COMMAND_COMMIT_FUSE: break;
	case REGD_COMMAND_MOTOR_START: break;
	case REGD_COMMAND_MOTOR_STOP: break;
	}

	REG_WRITE(REGD_COMMAND, REGD_COMMAND_NOP);
}

static void
_handle_reset(void)
{
	uint16_t ctrl = REG_READ16(REG_CONTROL_L);

	if (!(ctrl & REG_CONTROL_READY))
	{
		_reset();
	}
}

static void
_reset(void)
{
	uart_deinit();

	// Initialize registers
	for (size_t i = REG_VALUE_L; i < REGS_USER_TOP; i++)
		REG_WRITE(i, 0);
	regs_load_debug();

	// TODO - disable peripherals and reset pins so they can be
	// reinitialized

	_take_system_awake();

	uart_init(true, false, true); // TODO grab settings from registers

	REG_WRITE16(
		REG_CONTROL_L,
		REG_CONTROL_SWLOCK | REG_CONTROL_READY
	);
}

static void
_take_system_awake(void)
{
	ccp_write_io((uint8_t *) &CLKCTRL.MCLKCTRLA, CLKCTRL_CLKSEL_OSC20M_gc);

	ccp_write_io((uint8_t *) &CLKCTRL.MCLKCTRLB,
#if (F_CPU == 16000000) || (F_CPU == 20000000)
		0
#elif (F_CPU == 8000000) || (F_CPU == 10000000)
		CLKCTRL_PDIV_2X_gc | CLKCTRL_PEN_bm
#elif (F_CPU == 4000000) || (F_CPU == 5000000)
		CLKCTRL_PDIV_4X_gc | CLKCTRL_PEN_bm
#elif (F_CPU == 2000000) || (F_CPU == 2500000)
		CLKCTRL_PDIV_8X_gc | CLKCTRL_PEN_bm
#elif (F_CPU == 1000000) || (F_CPU == 1250000)
		CLKCTRL_PDIV_16X_gc | CLKCTRL_PEN_bm
#else
#  error "unexpected CPU frequency"
#endif
	);

	sei();
}
