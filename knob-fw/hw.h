// intellectual property is bullshit bgdc

#ifndef HW_H
#define HW_H

#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>
#include <xpins.h>

#define UART_DEFAULT_BAUD 9600

#define P_UART_TX     XPA1
#define P_UART_RX     XPA2
#define P_UART_TX_ALT XPB2

#define P_ADDR        XPA3
#define P_SLEEP       XPA4
#define P_QUAD0       XPA5
#define P_THRESH      XPA6
#define P_QUAD1       XPA7
#define P_MOT1        XPB0
#define P_MOT2        XPB1
#define P_nINT        XPB3

#endif // !defined(HW_H)
