// © by xan. All rights reserved. See accompanying COPYING.md for more info.

#ifndef FLETCHER16_H
#define FLETCHER16_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>

uint16_t fletcher16(uint8_t const * data, size_t len);

#ifdef __cplusplus
}
#endif

#endif // !defined(FLETCHER16_H)
