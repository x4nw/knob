// © by xan. All rights reserved. See accompanying COPYING.md for more info.

#include "fletcher16.h"

#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>

uint16_t
fletcher16(uint8_t const * data, size_t len)
{
	uint16_t sum1 = 0;
	uint16_t sum2 = 0;

	for (size_t i = 0; i < len; i++)
	{
		sum1 = sum1 + data[i];
		while (sum1 >= 255) sum1 -= 255;
		sum2 = sum2 + sum1;
		while (sum2 >= 255) sum2 -= 255;
	}

	return (sum2 << 8) | sum1;
}
