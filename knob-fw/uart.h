#ifndef UART_H
#define UART_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>

// The UART protocol accesses the same register map as the I2C protocol.
// It uses 8O1 (eight bits, odd parity, 1 stop bit).
//
// Each byte is either a hex nibble or a control character. If a hex nibble,
// it is pushed into the binary receive buffer, which can accumulate up to 16
// bits, received most significant first (big endian). The value of the
// accumulator is undefined after a command that uses it. Control characters
// indicate what to do with the value in the buffer.
//
// Hex characters must be uppercase; lowercase letters are control characters.
//
// Control characters:
//   d    device address: disregard everything until the next address match
//        command unless the local address matches the buffer. A zero address
//        broadcasts (note that a device in broadcast mode will not transmit,
//        to avoid bus contention! This may be used to recover a device with
//        its address unknown.)
//   p    ping - cause the selected device to send an ACK
//   +    no-op. sent by the server to acknowledge a write or a ping
//   -    error was received. no commands will be processed until c is sent
//   c    clear error state
//   a    store N as address pointer
//   r    request N bytes to be read starting at the address pointer
//   w    write 1 byte starting at the address pointer from the low part of N
//        and ACK
//   W    write 2 bytes starting at the address pointer, high part of N first
//        and ACK
//   .    sent by the server to indicate end of bytes read
//   !    indicates a debug packet. Terminated by .
//
// Write 00 11 22 33 starting at address 0x12 on device 0x02:
//   To server:   02d 12a0011W 2233W
//   From server:    +        +     +
//
// Read 8 bytes starting at address 0x8E on device 0x04:
//   To server:   04d 8Ea08r
//   From server:    +      0011223344556677.
//
// Debug strings are sent with an 8-bit key and a 16-bit value, and will never
// interrupt a response from a command.
//   From server: !kkvvvv.

// Initialize the UART driver. If there's any chance it was previously
// initialized, you should always call uart_deinit() first.
//
// rxen:         whether the receiver is enabled. When using I2C, this must be
//               false as there is no alternate location receive port.
// alternate_tx: whether the transmitter is diverted to the alternate location.
// debug_en:     whether debug info is to be emitted.
void uart_init(bool rxen, bool alternate_tx, bool debug_en);

// Deinitialize the UART driver.
void uart_deinit(void);

// Handle the last packet received, if there was one, and transmit debug info
// if there is any queued.
void uart_handle(void);

// Add a debug item to the queue. If the queue is full, it will be silently
// dropped.
void uart_debug(uint8_t key, uint16_t value);

#ifdef __cplusplus
}
#endif

#endif // !defined(UART_H)
