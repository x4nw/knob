#include "regs.h"
#include "debug_codes.h"
#include "uart.h"
#include "fletcher16.h"

#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>
#include <avr/eeprom.h>

uint8_t regmap_main[REGS_USER_TOP];
uint8_t regmap_debug[REGS_DEBUG_TOP - 0x80];

const uint8_t regmap_default[REGS_DEBUG_TOP - 0x80] = {
	[REGD_COMMAND - 0x80]      = 0,
	[REGD_SETTINGS - 0x80]     = 0,
	[REGD_BAUD - 0x80]         = REGD_BAUD_9600,
	[REGD_DAC_SETPOINT - 0x80] = 0,
};

// KNOB
#define EEP_MAGIC UINT32_C(0x424F4E48)

typedef struct regmap_store_s
{
	uint32_t magic;
	uint16_t csum;
	uint8_t  regmap[REGS_DEBUG_TOP - 0x80];
} regmap_store_t;

EEMEM regmap_store_t regmap_store;

static void _regs_default(void);

// Format in EEPROM:
// {'K', 'N', 'O', 'B', regmap_debug, checksum}
// checksum is fletcher16(regmap_debug)+1 (the +1 means that both 0 and 65535
// are not valid checksums, allowing false valids for cleared eeprom)

void
regs_load_debug(void)
{
	uint32_t magic = eeprom_read_dword(&regmap_store.magic);
	if (magic != EEP_MAGIC)
	{
		// Called before UART init and uart_debug() can't do that yet
		//uart_debug(DBG_EEPSTAT, DBG_EEPSTAT_BAD_MAGIC);
		_regs_default();
		return;
	}

	eeprom_read_block(regmap_debug, &regmap_store.regmap[0], sizeof(regmap_debug));
	uint16_t csum_real = fletcher16(regmap_debug, sizeof(regmap_debug));
	uint16_t csum_claimed = eeprom_read_word(&regmap_store.csum);
	REG_WRITE(REGD_COMMAND, REGD_COMMAND_NOP);

	if (csum_real != csum_claimed)
	{
		//uart_debug(DBG_EEPSTAT, DBG_EEPSTAT_BAD_CSUM);
		_regs_default();
	}
}

static void
_regs_default(void)
{
	for (uint8_t i = 0; i < sizeof(regmap_debug); i++)
		regmap_debug[i] = regmap_default[i];
}

void
regs_commit_debug(void)
{
	REG_WRITE(REGD_COMMAND, REGD_COMMAND_NOP);
	uint16_t csum = fletcher16(regmap_debug, sizeof(regmap_debug));
	uart_debug(DBG_EEPCSUM, csum);
	uart_debug(DBG_EEPSTAT, DBG_EEPSTAT_WRITING);

	eeprom_update_dword(&regmap_store.magic, EEP_MAGIC);
	eeprom_update_block(regmap_debug, &regmap_store.regmap[0], sizeof(regmap_debug));
	eeprom_update_word(&regmap_store.csum, csum);
}

uint8_t
reg_read(uint8_t addr)
{
	if (addr < REGS_USER_TOP)
		return regmap_main[addr];
	else if (addr >= 0x80 && addr < REGS_DEBUG_TOP)
		return regmap_debug[addr - 0x80];
	else
		return 0;
}

void
reg_write(uint8_t addr, uint8_t value)
{
	if (addr < REGS_USER_TOP)
		regmap_main[addr] = value;
	else if (addr >= 0x80 && addr < REGS_DEBUG_TOP)
		regmap_debug[addr - 0x80] = value;
}
