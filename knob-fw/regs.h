#ifndef REGS_H
#define REGS_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>

// You could open this generated header to see the definitions, but it's easier
// to just read regmap.py.
#include "regmap.h"

extern uint8_t regmap_main[REGS_USER_TOP];
extern uint8_t regmap_debug[REGS_DEBUG_TOP - 0x80];

// DEEP SLEEP:
// When REG_CONTROL_DEEPSLEEP is asserted, the system enters deep sleep. This
// is an ultra low power state where everything including the microcontroller
// itself is shut down. When asleep, the system can be started again by
// asserting nINT externally (thus, if multiple Knobs are on the same
// interrupt line, you should first turn off INTEN on all!).

// Load the debug register section from EEPROM
void regs_load_debug(void);

// Commit registers to EEPROM
void regs_commit_debug(void);

// Read a register at the given address. Safe to call with invalid address
// (will give 0 for undefined locations)
uint8_t reg_read(uint8_t addr);

// Write a register at the given address. Safe to call with invalid
// addr...addr+n (will not write those locations)
void reg_write(uint8_t addr, uint8_t value);

// Read a single 8-bit register and return it directly. Not bounds-checked!
#define REG_READ(addr) (addr < 0x80 \
	? regmap_main[addr] \
	: regmap_debug[(addr) - 0x80])

// Read a 16-bit register pair and return it directly. Not bounds-checked!
#define REG_READ16(addr) \
	((uint16_t)REG_READ(addr) | (uint16_t)((REG_READ((addr) + 1)) << 8))

// Write a single 8-bit register. Not bounds-checked!
#define REG_WRITE(addr, value) do { \
	if ((addr) < 0x80) { regmap_main[addr] = (value); } \
	else { regmap_debug[(addr) - 0x80] = (value); } } while (0)

// Write a 16-bit register pair. Not bounds-checked!
#define REG_WRITE16(addr, value) do { \
	REG_WRITE((addr), (value) & 0xFF); \
	REG_WRITE((addr) + 1, ((value) & 0xFF00) >> 8); } while (0)

#ifdef __cplusplus
}
#endif

#endif // !defined(REGS_H)
